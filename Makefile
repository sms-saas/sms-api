PWD = $(shell pwd)

bundle:
	@docker run --rm -v $(PWD)/api:/spec redocly/cli bundle api.yaml > swagger.yaml

html:
	@docker run --rm -v $(PWD):/spec redocly/cli build-docs swagger.yaml -o api.html

gen: bundle html

 .PHONY: bundle html gen